# GoBear

Android Technical Assessment.

## User Stories

The following **required** functionality is completed:

* Launch / App walkthrough ( 3 Screens )
    * [x] Allow user to skip onboarding when skip button has been clicked.
* User can login ( 1 Screen )
  * [x] Username.
  * [x] Password.
  * [x] Remember me call to action. 
  * [x] Field validation.
  * [x] Dummy credentials (No api).       
        ▪ Username: GoBear       
        ▪ Password : GoBearDemo
                o Store locally.
* User can see a news list ( 1 Screen )
  * [x] Consumes: http://feeds.bbci.co.uk/news/world/asia/rss.xml
  * [x] ListView:       
        ▪ Article title.       
        ▪ Article date.       
        ▪ Article small Image.       
        ▪ Article story intro.
  * [x] Pull to refresh. 
  * [x] Logout call to action.
* Allow user to view details of the news detail ( 1 Screen )
  * [x] Article's detail:       
        ▪ Article title.
        ▪ Article date.
        ▪ Article full image. 
        ▪ Article full story.


The following **additional** features are implemented:

* [x] Improve UI/UX with the using of PopupHelper, NetworkExceptions.
* [x] Follow and use new Android's architecture components:       
        ▪ Paging.       
        ▪ Navigation.       
        ▪ LiveData.       
        ▪ ViewModel.
* [x] Implement On boarding using Leanback.
* [x] Improve the application's performance with TDD(Instrument test).2


## Video Walkthrough

Here are two walkthrough's images of implemented user stories:

<img src='https://gitlab.com/alannguyen/gobear/blob/master/release/part1.gif' title='Video Walkthrough Part 1' width='' alt='Video Walkthrough' />
<img src='https://gitlab.com/alannguyen/gobear/blob/master/release/part2.gif' title='Video Walkthrough Part 2' width='' alt='Video Walkthrough' />


## Installation guide

Please install app for the first review at release folder.


## Open-source libraries used

* [x] Convert items with the [RssConverterFactory](https://github.com/faruktoptas/RetrofitRssConverterFactory).       
* [x] Load images with the [Glide](https://github.com/bumptech/glide).


## License

    Copyright [2018] [Alan]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.