package sg.vinova.gobear.builder

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar

fun toolbarFunctionQueue(init: FunctionBuilder.ToolbarBuilder.() -> Unit) =
    FunctionBuilder.ToolbarBuilder().apply(init).build()

interface FunctionBuilder {

    class ToolbarBuilder {
        private var funcQueue: ArrayList<((activity: AppCompatActivity?, toolbar: Toolbar?) -> Unit)>? = arrayListOf()

        fun func(messageFunc: ((activity: AppCompatActivity?, toolbar: Toolbar?) -> Unit)): ToolbarBuilder {
            this.funcQueue?.plusAssign(messageFunc)
            return this
        }

        fun build(): ArrayList<((activity: AppCompatActivity?, toolbar: Toolbar?) -> Unit)> {
            return this.funcQueue ?: arrayListOf()
        }
    }
}