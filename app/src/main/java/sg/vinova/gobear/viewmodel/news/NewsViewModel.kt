package sg.vinova.gobear.viewmodel.news

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.switchMap
import androidx.lifecycle.ViewModel
import sg.vinova.gobear.constant.PAGE_SIZE
import sg.vinova.gobear.repository.ListingBundle
import sg.vinova.gobear.repository.feature.news.NewsRepository
import sg.vinova.gobear.vo.feature.news.News
import sg.vinova.rssconverterfactory.RssFeed

class NewsViewModel(private val repository: NewsRepository.NonPaging) : ViewModel() {
    private val requestNewsItem = MutableLiveData<ListingBundle<Any?>?>()

    val newsItemLiveData = switchMap(requestNewsItem) { it?.pagedList }!!
    val networkState = switchMap(requestNewsItem) { it?.networkState }!!
    val dbState = switchMap(requestNewsItem) { it?.dbState }!!

    fun getNews() {
        repository.getNews(
            pageSize = PAGE_SIZE,
            request = requestNewsItem,
            handleResponseFunc = this::handleResponseFunction
        )
    }

    fun refresh() {
        requestNewsItem.value?.refresh?.invoke()
    }

    fun retry() {
        requestNewsItem.value?.retry?.invoke()
    }

    private fun handleResponseFunction(originalResponse: RssFeed?): List<Any>? {
        //convert here
        var finalResponse: List<News> = listOf()
        originalResponse?.data?.forEach {
            finalResponse += News(item = it)
        }

        return finalResponse
    }
}