package sg.vinova.gobear.viewmodel.news

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import sg.vinova.gobear.repository.feature.news.NewsRepository

class NewsViewModelFactory<T>(
    private val repository: T
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        when (modelClass) {
            NewsViewModel::class.java -> NewsViewModel(repository as NewsRepository.NonPaging) as T
            else -> NewsViewModel(repository as NewsRepository.NonPaging) as T
        }
}