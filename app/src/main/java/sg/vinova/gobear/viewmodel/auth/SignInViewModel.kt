package sg.vinova.gobear.viewmodel.auth

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.switchMap
import androidx.lifecycle.ViewModel
import sg.vinova.gobear.constant.UNKNOWN_ERROR
import sg.vinova.gobear.ext.checkIsNotEmpty
import sg.vinova.gobear.param.feature.auth.SignInParam
import sg.vinova.gobear.repository.ObjectBundle
import sg.vinova.gobear.repository.RepoState
import sg.vinova.gobear.repository.feature.auth.AuthRepository
import sg.vinova.gobear.vo.Token
import sg.vinova.gobear.vo.feature.auth.UserAuthentication

class SignInViewModel : ViewModel() {
    private val requestStringUserAuthentication = MutableLiveData<ObjectBundle<UserAuthentication>>()
    var username = ObservableField<String>("")
    var password = ObservableField<String>("")

    val userLiveData = switchMap(requestStringUserAuthentication) { it.objectLiveData }!!
    val errorLiveData = switchMap(requestStringUserAuthentication) { it.networkState }!!

    fun setLogin(signInParam: SignInParam, isRemember: Boolean) {
        if (AuthRepository.usernameValidator(signInParam.username.toLowerCase()) && AuthRepository.passwordValidator(
                signInParam.password
            )
        ) {
            val objectUserLiveData = MutableLiveData<UserAuthentication>()
            objectUserLiveData.value = UserAuthentication(
                username = signInParam.username,
                password = if (isRemember) signInParam.password else "",
                isRemember = isRemember,
                token = Token(signInParam.username + signInParam.password)
            )
            requestStringUserAuthentication.value = ObjectBundle(objectLiveData = objectUserLiveData)
        } else {
            val networkLiveData = MutableLiveData<RepoState>()
            networkLiveData.value = RepoState.error("Username or Password is invalid", UNKNOWN_ERROR)
            requestStringUserAuthentication.value = ObjectBundle(objectLiveData = MutableLiveData<UserAuthentication>(), networkState = networkLiveData)
        }
    }

    fun setActive(username: String, password: String): Boolean {
        return username.checkIsNotEmpty() && password.checkIsNotEmpty()
    }
}