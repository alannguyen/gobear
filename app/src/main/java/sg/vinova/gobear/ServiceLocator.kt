package sg.vinova.gobear

import android.app.Application
import sg.vinova.gobear.api.BBCServiceApi
import sg.vinova.gobear.repository.RepoType
import sg.vinova.gobear.repository.feature.news.NewsRepository
import sg.vinova.gobear.repository.inMemory.byListing.nonPaging.news.InMemoryByListingNonPagingNewsRepository
import java.util.concurrent.Executor
import java.util.concurrent.Executors

open class DefaultServiceLocator(val app: Application, val useInMemoryDb: Boolean) : IServiceLocator {

    // thread pool used for disk access
    @Suppress("PrivatePropertyName")
    private val DISK_IO = Executors.newSingleThreadExecutor()

    // thread pool used for network requests
    @Suppress("PrivatePropertyName")
    private val NETWORK_IO =
        Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 5)))

    private val api by lazy {
        BBCServiceApi.create(getNetworkExecutor())
    }

    override fun getNetworkExecutor(): Executor = NETWORK_IO
    override fun getDiskIOExecutor(): Executor = DISK_IO
    override fun getServiceApi(): BBCServiceApi = api

    override fun getNewsRepository(type: RepoType.Type): NewsRepository.NonPaging = when (type) {
        RepoType.Type.DB,
        RepoType.Type.IN_MEMORY_BY_LISTING_PAGING,
        RepoType.Type.IN_MEMORY_BY_OBJECT,
        RepoType.Type.IN_MEMORY_BY_LISTING_NON_PAGING -> InMemoryByListingNonPagingNewsRepository(
            apiService = getServiceApi(),
            networkExecutor = getNetworkExecutor()
        )
    }
}