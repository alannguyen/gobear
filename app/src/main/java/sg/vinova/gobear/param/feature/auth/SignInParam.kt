package sg.vinova.gobear.param.feature.auth

data class SignInParam(
    val username: String,
    val password: String
)