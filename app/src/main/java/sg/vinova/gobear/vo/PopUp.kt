package sg.vinova.gobear.vo

import android.app.Dialog
import android.os.Parcelable
import android.view.View
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class PopUp(
    var popupId: Int? = 0,
    var messageQueue: @RawValue ArrayList<(view: View?, dialog: Dialog?) -> Unit>? = null
) : Parcelable