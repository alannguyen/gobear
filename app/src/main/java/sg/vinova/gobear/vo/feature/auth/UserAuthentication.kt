package sg.vinova.gobear.vo.feature.auth

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import sg.vinova.gobear.vo.Token

@Entity()
@Parcelize
data class UserAuthentication(
    @PrimaryKey(autoGenerate = true)
    var userId: Int = 0,
    var username: String? = "",
    var password: String? = "",
    var isRemember: Boolean? = false,
    @Embedded
    var token: Token? = null,
    var isSkip: Boolean? = false
) : Parcelable