package sg.vinova.gobear.vo

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MetaData(
    val total_count: Int? = 0,
    val total_pages: Int? = 0,
    val next_page: Int? = 0,
    val prev_page: Int? = 0,
    val current_page: Int? = 0,
    val current_per_page: Int? = 0
) : Parcelable