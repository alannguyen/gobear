package sg.vinova.gobear.vo.feature.news

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import sg.vinova.rssconverterfactory.RssItem

@Entity()
@Parcelize
data class News(
    @PrimaryKey(autoGenerate = true)
    var newsId: Int = 0,
    @Embedded
    var item: RssItem? = RssItem()
) : Parcelable