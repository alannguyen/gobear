package sg.vinova.gobear.vo

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Token(
    var token: String? = "",
    var expired_at: String? = "",
    var refresh_token: String? = "",
    var revoked_at: String? = "",
    var created_at: String? = ""
) : Parcelable