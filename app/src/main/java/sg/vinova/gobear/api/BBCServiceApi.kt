package sg.vinova.gobear.api

import android.util.Log
import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.GET
import sg.vinova.gobear.BuildConfig
import sg.vinova.gobear.MainApplication
import sg.vinova.rssconverterfactory.RssConverterFactory
import sg.vinova.rssconverterfactory.RssFeed
import tech.linjiang.pandora.Pandora
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

interface BBCServiceApi {

    @GET("/news/world/asia/rss.xml")
    fun getNewsFeed(): Call<RssFeed>

    companion object {
        private const val BASE_URL = BuildConfig.SERVER_URL
        fun create(executor: Executor): BBCServiceApi = create(executor, HttpUrl.parse(BASE_URL)!!)
        private fun create(executor: Executor, httpUrl: HttpUrl): BBCServiceApi {
            val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
                Log.d("API", it)
            })
            logger.level = HttpLoggingInterceptor.Level.BODY

            val httpClient = OkHttpClient.Builder()
                .addInterceptor(logger)
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
            httpClient.addInterceptor { chain ->
                val original = chain.request()

                // Request customization: add request headers
                MainApplication.instance.updateCurrentUser()
                val token: String? = MainApplication.instance.currentUser?.token?.token

                val requestBuilder: Request.Builder?
                requestBuilder = original.newBuilder()

                val request = requestBuilder?.build()!!
                chain.proceed(request)
            }
            if (BuildConfig.DEBUG) {
                httpClient.addNetworkInterceptor(StethoInterceptor())
                    .addInterceptor(Pandora.get().interceptor)
            }
            return Retrofit.Builder()
                .baseUrl(httpUrl)
                .client(httpClient.build())
                .addConverterFactory(RssConverterFactory.create())
                .callbackExecutor(executor)
                .build()
                .create(BBCServiceApi::class.java)
        }
    }
}