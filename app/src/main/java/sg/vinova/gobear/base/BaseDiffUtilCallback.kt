package sg.vinova.gobear.base

data class BaseDiffUtilCallback<T>(
    var areItemsTheSameFunc: ((T, T) -> Boolean)? = null,
    var areContentsTheSameFunc: (T, T) -> Boolean,
    var getChangePayloadFunc: (T, T) -> T
)