package sg.vinova.gobear.base

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase

abstract class BaseDb : RoomDatabase() {
    companion object {

        @Volatile
        var INSTANCE: RoomDatabase? = null

        inline fun <reified T : RoomDatabase> getInstance(
            context: Context,
            useInMemory: Boolean,
            databaseName: String
        ): T? =
            INSTANCE as? T ?: synchronized(this) {
                create<T>(context, useInMemory, databaseName).also { INSTANCE = it }
            }

        inline fun <reified T : RoomDatabase> create(
            context: Context,
            useInMemory: Boolean,
            databaseName: String
        ): T? {
            val databaseBuilder = if (useInMemory) {
                Room.inMemoryDatabaseBuilder(context, T::class.java)
            } else {
                Room.databaseBuilder(context, T::class.java, databaseName)
            }
            return databaseBuilder
                .fallbackToDestructiveMigration()
                .build()
        }
    }

}