package sg.vinova.gobear

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.facebook.stetho.Stetho
import sg.vinova.gobear.ext.deleteUserPrefObj
import sg.vinova.gobear.ext.getUserPrefObj
import sg.vinova.gobear.ext.saveUserPrefObj
import sg.vinova.gobear.vo.feature.auth.UserAuthentication
import tech.linjiang.pandora.Pandora

class MainApplication : Application(), LifecycleObserver {
    var currentUser: UserAuthentication? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
        if (BuildConfig.DEBUG.not())
        else {
            Stetho.initializeWithDefaults(this)
            Pandora.get().open()
        }

        currentUser = getUserPrefObj()
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    companion object {
        lateinit var instance: MainApplication
            private set
        var isActive: Boolean = false
    }

    fun saveCurrentUser(user: UserAuthentication?) {
        saveUserPrefObj(user)
    }

    fun updateCurrentUser() {
        currentUser = getUserPrefObj()
    }

    fun deleteCurrentUser() {
        deleteUserPrefObj()
        currentUser = null
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        isActive = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        isActive = true
    }
}