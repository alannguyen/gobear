package sg.vinova.gobear.helper

import android.content.SharedPreferences
import com.google.gson.Gson
import sg.vinova.gobear.vo.feature.auth.UserAuthentication

class SharedPreferencesHelper(private val sharedPreferences: SharedPreferences) {
    fun getUser(): UserAuthentication? {
        val gson = Gson()
        return gson.fromJson<UserAuthentication>(
            sharedPreferences.getString("user", null),
            UserAuthentication::class.java
        )
    }

    fun saveUser(user: UserAuthentication?): Boolean {
        // Start a SharedPreferences transaction.
        val editor = sharedPreferences.edit()
        val gson = Gson()
        editor.putString("user", gson.toJson(user))
        editor.apply()

        // Commit changes to SharedPreferences.
        return editor.commit()
    }
}
