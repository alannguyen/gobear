package sg.vinova.gobear.util

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ProgressBar
import sg.vinova.gobear.R
import sg.vinova.gobear.vo.PopUp

const val POPUP_MODEL = "popup_model"

class PopupDialogFragment : androidx.fragment.app.DialogFragment() {

    private var popup: PopUp? = null
    private var popUpView: View? = null

    companion object {
        fun newInstance(popup: PopUp?): PopupDialogFragment =
            PopupDialogFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(POPUP_MODEL, popup)
                }
            }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        popup = arguments?.getParcelable(POPUP_MODEL)
        popUpView = layoutInflater.inflate(popup?.popupId ?: R.layout.layout_loading, container)
        return popUpView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (popup == null)
            return

        view.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE
        popup?.messageQueue?.forEach { it ->
            it.invoke(view, dialog)
        }
        view.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun show(manager: androidx.fragment.app.FragmentManager?, tag: String?) {
        val ft = manager?.beginTransaction()
        ft?.add(this, tag)
        ft?.commitAllowingStateLoss()
    }
}