package sg.vinova.gobear.util

import androidx.paging.PositionalDataSource
import java.util.*

class ListDataSource<T>(list: List<T>) : PositionalDataSource<T>() {
    private val mList: List<T>

    init {
        mList = ArrayList(list)
    }

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<T>) {
        val totalCount = mList.size
        val position = computeInitialLoadPosition(params, totalCount)
        val loadSize = computeInitialLoadSize(params, position, totalCount)
        // for simplicity, we could return everything immediately,
        // but we tile here since it's expected behavior
        val sublist = mList.subList(position, position + loadSize)
        callback.onResult(sublist, position, totalCount)
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<T>) {
        callback.onResult(
            mList.subList(
                params.startPosition,
                params.startPosition + if (params.loadSize + params.startPosition - 1 < mList.size) params.loadSize else 0
            )
        )
    }
}
