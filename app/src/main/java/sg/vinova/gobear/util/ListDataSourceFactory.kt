package sg.vinova.gobear.util

import androidx.paging.DataSource

class ListDataSourceFactory<T>(val list: List<T>?) : DataSource.Factory<Int, T>() {
    override fun create(): DataSource<Int, T> = ListDataSource<T>(list ?: emptyList())
}