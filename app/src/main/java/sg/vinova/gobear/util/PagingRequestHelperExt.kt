package sg.vinova.gobear.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingRequestHelper
import sg.vinova.gobear.repository.RepoState

private fun getErrorMessage(report: PagingRequestHelper.StatusReport): String {
    return PagingRequestHelper.RequestType.values().mapNotNull {
        report.getErrorFor(it)?.message
    }.first()
}

fun PagingRequestHelper.createStatusLiveData(): LiveData<RepoState> {
    val liveData = MutableLiveData<RepoState>()
    addListener { report ->
        when {
            report.hasRunning() -> liveData.postValue(RepoState.LOADING)
            report.hasError() -> {
                val error = getErrorMessage(report).split(" - ")
                if (error.size == 2)
                    liveData.postValue(RepoState.error(error[0], error[1].toInt()))
                else
                    liveData.postValue(RepoState.error(error[0]))
            }
            else -> liveData.postValue(RepoState.LOADED)
        }
    }
    return liveData
}
