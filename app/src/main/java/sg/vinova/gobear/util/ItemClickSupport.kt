package sg.vinova.gobear.util

import android.view.View
import android.view.View.OnLongClickListener
import sg.vinova.gobear.R

typealias OnItemClickListener = (recyclerView: androidx.recyclerview.widget.RecyclerView, curItemView: View, position: Int, id: Long) -> Unit
typealias OnItemLongClickListener = (recyclerView: androidx.recyclerview.widget.RecyclerView, curItemView: View, position: Int, id: Long) -> Boolean

class ItemClickSupport private constructor(private val recyclerView: androidx.recyclerview.widget.RecyclerView) {
    private var itemClickListener: OnItemClickListener? = null
    private var itemLongClickListener: OnItemLongClickListener? = null
    private var itemClickListenerMessageQueue: ArrayList<(view: View?) -> Unit>? = null
    private var itemLongClickListenerMessageQueue: ArrayList<(view: View?) -> Unit>? = null

    private val clickListener = View.OnClickListener { curItemView ->
        if (itemClickListener != null) {
            val position = recyclerView.getChildAdapterPosition(curItemView)
            val id = recyclerView.getChildItemId(curItemView)
            itemClickListener?.invoke(recyclerView, curItemView, position, id)
        }
    }

    private val longClickListener = OnLongClickListener { curItemView ->
        if (itemLongClickListener != null) {
            val position = recyclerView.getChildAdapterPosition(curItemView)
            val id = recyclerView.getChildItemId(curItemView)
            return@OnLongClickListener itemLongClickListener?.invoke(recyclerView, curItemView, position, id)
                ?: false
        }
        false
    }

    private val attachListener = object : androidx.recyclerview.widget.RecyclerView.OnChildAttachStateChangeListener {
        override fun onChildViewAttachedToWindow(view: View) {
            val position = recyclerView.getChildAdapterPosition(view)

            if (itemClickListener != null) {
                view.setOnClickListener(clickListener)
            }

            if (itemLongClickListener != null) {
                view.setOnLongClickListener(longClickListener)
            }

            itemClickListenerMessageQueue?.forEach {
                it.invoke(view)
            }

            itemLongClickListenerMessageQueue?.forEach {
                it.invoke(view)
            }
        }

        override fun onChildViewDetachedFromWindow(view: View) {
            if (itemClickListener != null) {
                view.setOnClickListener(null)
            }

            if (itemLongClickListener != null) {
                view.setOnLongClickListener(null)
            }

            itemClickListenerMessageQueue?.clear()
            itemLongClickListenerMessageQueue?.clear()
        }
    }

    private fun attach(view: androidx.recyclerview.widget.RecyclerView) {
        view.setTag(R.id.item_click_support, this)
        view.addOnChildAttachStateChangeListener(attachListener)
    }

    private fun detach(view: androidx.recyclerview.widget.RecyclerView) {
        view.removeOnChildAttachStateChangeListener(attachListener)
        view.setTag(R.id.item_click_support, null)
        itemClickListener = null
        itemLongClickListener = null
        itemClickListenerMessageQueue = null
        itemLongClickListenerMessageQueue = null
    }

    fun setOnItemClickListener(listener: OnItemClickListener): ItemClickSupport {
        itemClickListener = listener
        return this
    }

    fun setOnItemLongClickListener(listener: OnItemLongClickListener): ItemClickSupport {
        itemLongClickListener = listener
        return this
    }

    fun setOnItemParentAndChildrenClickListener(
        listener: OnItemClickListener?,
        messageQueue: ArrayList<(curItemView: View?) -> Unit>?
    ): ItemClickSupport {
        itemClickListener = listener
        itemClickListenerMessageQueue = messageQueue
        return this
    }

    fun setOnItemParentAndChildrenLongClickListener(
        listener: OnItemLongClickListener,
        messageQueue: ArrayList<(curItemView: View?) -> Unit>?
    ): ItemClickSupport {
        itemLongClickListener = listener
        itemLongClickListenerMessageQueue = messageQueue
        return this
    }

    companion object {

        fun addTo(view: androidx.recyclerview.widget.RecyclerView): ItemClickSupport {
            var support: ItemClickSupport? = view.getTag(R.id.item_click_support) as? ItemClickSupport
            if (support == null) {
                support = ItemClickSupport(view)
                support.attach(view)
            } else {
            }
            return support
        }

        fun removeFrom(view: androidx.recyclerview.widget.RecyclerView): ItemClickSupport? {
            val support = view.getTag(R.id.item_click_support) as? ItemClickSupport
            support?.detach(view)
            return support
        }
    }
}