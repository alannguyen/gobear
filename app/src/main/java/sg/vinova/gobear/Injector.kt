package sg.vinova.gobear

import android.content.Context
import sg.vinova.gobear.repository.RepoType
import sg.vinova.gobear.repository.feature.news.NewsRepository
import sg.vinova.gobear.viewmodel.auth.SignInViewModelFactory
import sg.vinova.gobear.viewmodel.news.NewsViewModelFactory

object Injector {

    fun provideSignInViewModelFactory(): SignInViewModelFactory {
        return SignInViewModelFactory()
    }

    fun provideNewsViewModelFactory(
        context: Context, repoType: RepoType.Type
    ): NewsViewModelFactory<NewsRepository.NonPaging> {
        val repo = IServiceLocator.instance(context)
            .getNewsRepository(repoType)
        return NewsViewModelFactory(repo)
    }
}

