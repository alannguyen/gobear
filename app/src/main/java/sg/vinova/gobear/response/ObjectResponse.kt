package sg.vinova.gobear.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ObjectResponse<T> : BaseResponse<T>(), Parcelable