package sg.vinova.gobear.response

import android.os.Parcelable
import sg.vinova.gobear.vo.MetaData

abstract class BaseResponse<T>(
    var status: Boolean? = null,
    var message: String? = null,
    var metadata: MetaData? = null,
    var code: Int? = null,
    open var data: T? = null
) : Parcelable {
    fun isSuccess() = status == true
}