package sg.vinova.gobear.exception

import sg.vinova.gobear.constant.*
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class NetworkErrorException(message: String? = "", val code: Int? = null) : Exception(message) {
    companion object {
        val NO_NETWORK = NetworkErrorException(REQUEST_TIMEOUT)
        val NO_CONNECT_TO_SERVER = NetworkErrorException(CONNECT_ERROR)
    }

    fun errors(t: Throwable?, errorMessage: String? = ""): Throwable {
        return when (t) {
            is UnknownHostException, is SocketTimeoutException ->
                Throwable("$INTERNET_ERROR - $NO_INTERNET_CONNECTION")
            is ConnectException -> Throwable("$CONNECT_ERROR -| $NO_SERVER_CONNECTION")
            else -> t ?: Throwable(errorMessage ?: "$SERVER_ERROR - $UNKNOWN_ERROR")
        }
    }
}