package sg.vinova.gobear.db.feature.auth

import androidx.room.Database
import sg.vinova.gobear.base.BaseDb
import sg.vinova.gobear.db.dao.UserAuthenticationDao
import sg.vinova.gobear.vo.feature.auth.UserAuthentication

@Database(
    entities = [UserAuthentication::class],
    version = 1,
    exportSchema = true
)
abstract class AuthServiceDb : BaseDb() {
    abstract fun userGoBear(): UserAuthenticationDao
}