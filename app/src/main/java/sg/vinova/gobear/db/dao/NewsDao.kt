package sg.vinova.gobear.db.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import sg.vinova.gobear.vo.feature.news.News

@Dao
interface NewsDao : BaseDao<News> {
    @Query("DELETE FROM News")
    fun delete()

    @Query("SELECT * FROM News ORDER BY newsId DESC")
    fun get(): DataSource.Factory<Int, News>
}