package sg.vinova.gobear.db.feature.news

import androidx.room.Database
import sg.vinova.gobear.base.BaseDb
import sg.vinova.gobear.db.dao.NewsDao
import sg.vinova.gobear.vo.feature.news.News

@Database(
    entities = [News::class],
    version = 1,
    exportSchema = false
)
abstract class NewsDb : BaseDb() {
    abstract fun news(): NewsDao
}