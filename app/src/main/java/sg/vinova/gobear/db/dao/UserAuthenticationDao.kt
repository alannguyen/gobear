package sg.vinova.gobear.db.dao

import androidx.room.Dao
import androidx.room.Query
import sg.vinova.gobear.vo.feature.auth.UserAuthentication

@Dao
interface UserAuthenticationDao : BaseDao<UserAuthentication> {
    @Query("SELECT * FROM UserAuthentication")
    fun get(): UserAuthentication
}