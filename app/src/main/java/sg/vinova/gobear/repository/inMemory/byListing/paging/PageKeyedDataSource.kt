package sg.vinova.gobear.repository.inMemory.byListing.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sg.vinova.gobear.MainApplication
import sg.vinova.gobear.constant.*
import sg.vinova.gobear.exception.NetworkErrorException
import sg.vinova.gobear.repository.RepoState
import sg.vinova.gobear.response.ListObjectResponse
import sg.vinova.gobear.response.ObjectResponse
import sg.vinova.gobear.vo.feature.auth.UserAuthentication
import java.util.concurrent.Executor

class PageKeyedDataSource<T, K>(
    private val originalCall: Call<ListObjectResponse<T>>,
    private val networkIOExecutor: Executor,
    private var retryCall: Call<ObjectResponse<K>>? = null,
    private val loadAfter: (params: LoadParams<Int>) -> Call<ListObjectResponse<T>>,
    private val handleResponseFunc: ((params: List<T>?) -> List<Any>?)? = null
) : PageKeyedDataSource<Int, Any>() {

    // keep a function reference for the retry event
    private var retry: (() -> Any?)? = null

    val networkState = MutableLiveData<RepoState>()

    fun retryAllFailed() {
        val prevRetry = retry
        prevRetry?.let {
            networkIOExecutor.execute {
                it.invoke()
            }
        }
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Any>
    ) {
        retry = { loadInitial(params, callback) }
        val request = originalCall.clone()
        networkState.postValue(RepoState.LOADING)

        // triggered by a refresh, we better execute sync
        try {
            val response = request.execute().body()
            when {
                (response?.code == null && response?.message == null) -> {
                    networkIOExecutor.execute {
                        val data =
                            if (handleResponseFunc != null) handleResponseFunc.invoke(response?.data) else response?.data
                        callback.onResult(data ?: return@execute, null, response?.metadata?.next_page)
                        retry = null
                        networkState.postValue(RepoState.LOADED)
                    }
                }
                (response.code == OAUTH_TOKEN_INVALID_OR_EXPIRED ||
                        response.code == ACCESS_TOKEN_NOT_PROVIDE ||
                        response.code == ACCESS_TOKEN_INVALID ||
                        response.code == REFRESH_TOKEN_INVALID) -> {
                    retryCall?.clone()
                        ?.enqueue(RetryCallback(params, null, callback, null, response.message, response.code))
                }
                else -> {
                    val error = NetworkErrorException().errors(Throwable(response.message)).message?.split(" - ")
                    networkState.postValue(
                        RepoState.error(
                            error?.get(0), response.code
                        )
                    )
                }
            }
        } catch (ioException: Throwable) {
            val error = NetworkErrorException().errors(Throwable(ioException.message)).message
            networkState.postValue(
                RepoState.error(
                    error,
                    UNKNOWN_ERROR
                )
            )
        }
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, Any>
    ) {
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Any>) {
        networkState.postValue(RepoState.LOADING)
        retry = {
            loadAfter(params, callback)
        }

        loadAfter.invoke(params).enqueue(
            AfterCallback(
                params = params,
                callback = callback
            )
        )
    }

    inner class AfterCallback(
        private var params: LoadParams<Int>,
        private var callback: LoadCallback<Int, Any>
    ) :
        Callback<ListObjectResponse<T>> {
        override fun onFailure(call: Call<ListObjectResponse<T>>?, t: Throwable?) {
            val error = NetworkErrorException().errors(t).message?.split(" - ")
            if (retryCall?.isCanceled == false && retryCall?.isExecuted == false) {
                retryCall?.enqueue(
                    RetryCallback(
                        paramsInitial = null,
                        paramsAfter = params,
                        callbackInitial = null,
                        callbackAfter = callback,
                        errorMessage = t?.message,
                        errorCode = error?.get(1)?.toInt()
                    )
                )
            } else {
                networkState.postValue(RepoState.error(error?.get(0), error?.get(1)?.toInt()))
            }
        }

        override fun onResponse(
            executedCall: Call<ListObjectResponse<T>>?,
            response: Response<ListObjectResponse<T>>?
        ) {
            if (response?.isSuccessful == true) {
                val data = response.body()?.data

                when {
                    (response.body()?.code == null && response.body()?.message == null) -> {
                        networkIOExecutor.execute {
                            val finalData = if (handleResponseFunc != null) handleResponseFunc.invoke(data) else data
                            callback.onResult(finalData ?: return@execute, params.key + 1)
                            networkState.postValue(RepoState.LOADED)
                            retry = null
                        }
                    }

                    ((response.body()?.code == OAUTH_TOKEN_INVALID_OR_EXPIRED ||
                            response.body()?.code == ACCESS_TOKEN_NOT_PROVIDE ||
                            response.body()?.code == ACCESS_TOKEN_INVALID ||
                            response.body()?.code == REFRESH_TOKEN_INVALID)
                            && (retryCall?.isCanceled == false && retryCall?.isExecuted == false)) -> {
                        retryCall?.enqueue(
                            RetryCallback(
                                paramsInitial = null,
                                paramsAfter = params,
                                callbackInitial = null,
                                callbackAfter = callback,
                                errorMessage = response.body()?.message,
                                errorCode = response.body()?.code
                            )
                        )
                    }
                    else -> {
                        val error = RepoState.error(response.body()?.message ?: "unknown error", response.body()?.code)
                        networkState.postValue(error)
                    }
                }
            }
        }
    }

    inner class RetryCallback<K>(
        private val paramsInitial: LoadInitialParams<Int>? = null,
        private val paramsAfter: LoadParams<Int>? = null,
        private val callbackInitial: LoadInitialCallback<Int, Any>? = null,
        private val callbackAfter: LoadCallback<Int, Any>? = null,
        private val errorMessage: String?,
        private val errorCode: Int? = UNKNOWN_ERROR
    ) : Callback<ObjectResponse<K>> {
        override fun onFailure(call: Call<ObjectResponse<K>>?, t: Throwable?) {
            networkState.postValue(RepoState.error(errorMessage, errorCode))
            cancel()
        }

        override fun onResponse(
            executedCall: Call<ObjectResponse<K>>?,
            response: Response<ObjectResponse<K>>?
        ) {
            if ((response?.isSuccessful == true) && response.body()?.isSuccess() == true) {
                if (!originalCall.isCanceled && !originalCall.isExecuted) {
                    if (response.body() is ObjectResponse<K>)
                        MainApplication.instance.saveCurrentUser(response.body()?.data as? UserAuthentication)
                    if (paramsInitial != null && callbackInitial != null) {
                        loadInitial(paramsInitial, callbackInitial)
                    } else if (paramsAfter != null && callbackAfter != null) {
                        loadAfter(paramsAfter, callbackAfter)
                    }
                }
            } else {
                networkState.postValue(RepoState.error(errorMessage ?: "unknown err", errorCode))
                cancel()
            }
        }
    }

    fun cancel() = if (!originalCall.isCanceled) originalCall.cancel() else Unit
}