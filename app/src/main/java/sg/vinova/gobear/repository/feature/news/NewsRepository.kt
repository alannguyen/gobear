package sg.vinova.gobear.repository.feature.news

import androidx.lifecycle.MutableLiveData
import sg.vinova.gobear.repository.ListingBundle
import sg.vinova.rssconverterfactory.RssFeed

class NewsRepository {
    interface NonPaging {
        fun getNews(
            pageSize: Int,
            request: MutableLiveData<ListingBundle<Any?>?>? = null,
            handleResponseFunc: ((params: RssFeed?) -> List<Any>?)? = null
        )
    }
}