package sg.vinova.gobear.repository.inDb.byListing.nonPaging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sg.vinova.gobear.MainApplication
import sg.vinova.gobear.base.BaseDb
import sg.vinova.gobear.constant.*
import sg.vinova.gobear.exception.NetworkErrorException
import sg.vinova.gobear.ext.toListingBundle
import sg.vinova.gobear.repository.ListingBundle
import sg.vinova.gobear.repository.RepoState
import sg.vinova.gobear.response.BaseResponse
import sg.vinova.gobear.response.ListObjectResponse
import sg.vinova.gobear.response.ObjectResponse
import sg.vinova.gobear.vo.feature.auth.UserAuthentication
import java.util.concurrent.Executor

class WebServiceByListingNonPagingCallBack<T, K>(
    private val call: Call<ListObjectResponse<T>>,
    private val db: BaseDb? = null,
    private val request: MutableLiveData<ListingBundle<Any?>?>? = null,
    private var retry: Retry<ObjectResponse<K>>? = null,
    private val initializeFunc: () -> List<Any>?,
    private val handleResponseFunc: ((params: List<T>?) -> List<Any>?)? = null,
    private val handleFinalResponseFunc: (data: List<Any?>?) -> Unit,
    private val diskIOExecutor: Executor
) : MutableLiveData<ListObjectResponse<Any>>(), Callback<ListObjectResponse<T>> {

    val networkState = MutableLiveData<RepoState>()
    val dbState = MutableLiveData<RepoState>()

    @Volatile
    var result: List<Any?>? = null

    private fun updateResponse() {
        synchronized(this) {
            //            Log.d("alan", "response" + (result as? UserAuthentication)?.toString())
            request?.postValue(
                result?.toListingBundle(
                    networkState = networkState,
                    dbState = dbState,
                    retryFunc = { onActive() },
                    refreshFunc = { onActive() })
            )
        }
    }

    fun run() {
        request?.postValue(
            ListingBundle(
                pagedList = MutableLiveData<PagedList<Any?>>(),
                networkState = networkState,
                dbState = dbState,
                retry = { onActive() },
                refresh = { onActive() })
        )
        onActive()
    }

    override fun onActive() {
        //Default is main thread
        diskIOExecutor.execute {
            result = initializeFunc()
            updateResponse()
        }

        networkState.postValue(RepoState.LOADING)
        call.clone().enqueue(this)
    }

    override fun onFailure(executedCall: Call<ListObjectResponse<T>>?, t: Throwable?) {
        val error = NetworkErrorException().errors(t).message?.split(" - ")
        if (error?.get(1)?.toInt() == NO_INTERNET_CONNECTION || error?.get(1)?.toInt() == NO_SERVER_CONNECTION)
            networkState.postValue(RepoState.error(error[0], error[1].toInt()))
        else
            retry?.retryFunc?.clone()?.enqueue(RetryCallback(t?.message))
    }

    override fun onResponse(executedCall: Call<ListObjectResponse<T>>?, response: Response<ListObjectResponse<T>>?) {
        if ((response?.isSuccessful == true) && (response.body()?.isSuccess() == true)) {
            networkState.postValue(RepoState.LOADED)
            val finalData = if (handleResponseFunc != null) handleResponseFunc.invoke(
                response.body()?.data ?: return
            ) else response.body()?.data

            diskIOExecutor.execute {
                db ?: return@execute
                val result = db.runCatching {
                    dbState.postValue(RepoState.LOADING)
                    handleFinalResponseFunc(finalData)
                }
                result.onFailure {
                    dbState.postValue(RepoState.error(it.message))
                }
                result.onSuccess {
                    dbState.postValue(RepoState.LOADED)
                }.getOrThrow()
            }

            networkState.postValue(RepoState.LOADED)
            result = finalData
            updateResponse()

            retry?.retryFunc = null
        } else {
            if (response?.body()?.data != null &&
                response.body()?.code != OAUTH_TOKEN_INVALID_OR_EXPIRED &&
                response.body()?.code != ACCESS_TOKEN_NOT_PROVIDE &&
                response.body()?.code != ACCESS_TOKEN_INVALID &&
                response.body()?.code != REFRESH_TOKEN_INVALID
            ) {
                networkState.postValue(RepoState.LOADED)
            } else
                retry?.retryFunc?.clone()?.enqueue(RetryCallback(response?.body()?.message, response?.body()?.code))
        }
    }

    open class Retry<K>(var retryFunc: Call<K>? = null)

    inner class RetryCallback<K>(private val errorMessage: String?, private val errorCode: Int? = UNKNOWN_ERROR) :
        Callback<ObjectResponse<K>> {
        override fun onFailure(call: Call<ObjectResponse<K>>?, t: Throwable?) {
            networkState.postValue(RepoState.error(errorMessage ?: "unknown err", errorCode))
        }

        override fun onResponse(executedCall: Call<ObjectResponse<K>>?, response: Response<ObjectResponse<K>>?) {
            if ((response?.isSuccessful == true) && ((response.body() as? BaseResponse<*>)?.isSuccess() == true)) {
                if (response.body() is ObjectResponse<K>)
                    MainApplication.instance.saveCurrentUser(response.body()?.data as? UserAuthentication)
                call.clone().enqueue(this@WebServiceByListingNonPagingCallBack)
            } else {
                networkState.postValue(RepoState.error(errorMessage ?: "unknown err", errorCode))
            }
        }
    }
}