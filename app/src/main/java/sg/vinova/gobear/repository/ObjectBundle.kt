package sg.vinova.gobear.repository

import androidx.lifecycle.LiveData

data class ObjectBundle<T>(
    val objectLiveData: LiveData<T>,
    val networkState: LiveData<RepoState>? = null,
    val dbState: LiveData<RepoState>? = null
)