package sg.vinova.gobear.repository.inMemory.byListing.nonPaging.news

import androidx.lifecycle.MutableLiveData
import sg.vinova.gobear.api.BBCServiceApi
import sg.vinova.gobear.repository.ListingBundle
import sg.vinova.gobear.repository.feature.news.NewsRepository
import sg.vinova.gobear.repository.inMemory.byListing.nonPaging.RetrofitByListingLiveData
import sg.vinova.gobear.vo.feature.auth.UserAuthentication
import sg.vinova.rssconverterfactory.RssFeed
import java.util.concurrent.Executor

class InMemoryByListingNonPagingNewsRepository(
    private val apiService: BBCServiceApi,
    private val networkExecutor: Executor
) : NewsRepository.NonPaging {
    override fun getNews(
        pageSize: Int,
        request: MutableLiveData<ListingBundle<Any?>?>?,
        handleResponseFunc: ((params: RssFeed?) -> List<Any>?)?
    ) {
        networkExecutor.execute {
            RetrofitByListingLiveData<RssFeed, UserAuthentication>(
                pageSize = pageSize, call = apiService.getNewsFeed(),
                request = request,
                handleResponseFunc = handleResponseFunc
            ).run()
        }
    }
}