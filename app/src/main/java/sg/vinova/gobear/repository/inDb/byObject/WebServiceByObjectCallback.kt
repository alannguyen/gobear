package sg.vinova.gobear.repository.inDb.byObject

import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sg.vinova.gobear.MainApplication
import sg.vinova.gobear.base.BaseDb
import sg.vinova.gobear.constant.*
import sg.vinova.gobear.exception.NetworkErrorException
import sg.vinova.gobear.repository.RepoState
import sg.vinova.gobear.response.BaseResponse
import sg.vinova.gobear.response.ObjectResponse
import sg.vinova.gobear.vo.feature.auth.UserAuthentication
import java.util.concurrent.Executor

class WebServiceByObjectCallback<T, K>(
    private val call: Call<ObjectResponse<T>>,
    private val networkState: MutableLiveData<RepoState>? = null,
    private val dbState: MutableLiveData<RepoState>? = null,
    private val db: BaseDb? = null,
    private var retry: Retry<ObjectResponse<K>>? = null,
    private val initializeFunc: () -> Any?,
    private val handleResponseFunc: ((param: T?) -> Any?)? = null,
    private val handleFinalResponseFunc: (data: Any?) -> Unit,
    private val diskIOExecutor: Executor
) : MutableLiveData<Any>(), Callback<ObjectResponse<T>> {

    @Volatile
    var result: Any? = null

    private fun updateResponse() {
        synchronized(this) {
            //            Log.d("alan", "response" + (result as? UserAuthentication)?.toString())
            postValue(result)
        }
    }

    override fun onActive() {
        //Default is main thread
        diskIOExecutor.execute {
            result = initializeFunc()
            updateResponse()
        }

        if (!call.isCanceled && !call.isExecuted) {
            networkState?.postValue(RepoState.LOADING)
            call.clone().enqueue(this)
        }
    }

    override fun onFailure(executedCall: Call<ObjectResponse<T>>?, t: Throwable?) {
        val networkErrorException = NetworkErrorException().errors(t).message?.split(" - ")
        if (networkErrorException?.get(1)?.toInt() == NO_INTERNET_CONNECTION || networkErrorException?.get(1)?.toInt() == NO_SERVER_CONNECTION)
            networkState?.postValue(RepoState.error(networkErrorException[0], networkErrorException[1].toInt()))
        else
            retry?.retryFunc?.clone()?.enqueue(RetryCallback(t?.message))
    }

    override fun onResponse(executedCall: Call<ObjectResponse<T>>?, response: Response<ObjectResponse<T>>?) {
        if ((response?.isSuccessful == true) && (response.body()?.isSuccess() == true)) {
            val finalData = if (handleResponseFunc != null) handleResponseFunc.invoke(
                response.body()?.data ?: return
            ) else response.body()?.data
            diskIOExecutor.execute {
                db ?: return@execute
                val result = db.runCatching {
                    dbState?.postValue(RepoState.LOADING)
                    handleFinalResponseFunc(finalData)
                }
                result.onFailure {
                    dbState?.postValue(RepoState.error(it.message))
                }
                result.onSuccess {
                    dbState?.postValue(RepoState.LOADED)
                }.getOrThrow()
            }
            networkState?.postValue(RepoState.LOADED)
            result = finalData
            updateResponse()
            call.cancel()
            retry?.retryFunc = null
        } else {
            if (response?.body()?.data != null &&
                response.body()?.code != OAUTH_TOKEN_INVALID_OR_EXPIRED &&
                response.body()?.code != ACCESS_TOKEN_NOT_PROVIDE &&
                response.body()?.code != ACCESS_TOKEN_INVALID &&
                response.body()?.code != REFRESH_TOKEN_INVALID
            ) {
                networkState?.postValue(RepoState.LOADED)
                cancel()
            } else
                retry?.retryFunc?.enqueue(RetryCallback(response?.body()?.message))
        }
    }

    fun cancel() = if (!call.isCanceled) call.cancel() else Unit
    open class Retry<K>(var retryFunc: Call<K>? = null)

    inner class RetryCallback<K>(private val errorMessage: String?) : Callback<ObjectResponse<K>> {
        override fun onFailure(call: Call<ObjectResponse<K>>?, t: Throwable?) {
            networkState?.postValue(RepoState.error(errorMessage ?: "unknown err"))
            cancel()
        }

        override fun onResponse(executedCall: Call<ObjectResponse<K>>?, response: Response<ObjectResponse<K>>?) {
            if ((response?.isSuccessful == true) && ((response.body() as? BaseResponse<*>)?.isSuccess() == true)) {
                if (!call.isCanceled && !call.isExecuted) {
                    if (response.body() is ObjectResponse<K>)
                        MainApplication.instance.saveCurrentUser(response.body()?.data as? UserAuthentication)
                    call.enqueue(this@WebServiceByObjectCallback)
                }
                retry?.retryFunc = null
            } else {
                networkState?.postValue(RepoState.error(errorMessage ?: "unknown err"))
                cancel()
            }
        }
    }
}
