package sg.vinova.gobear.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagedList

data class ListingBundle<T>(
    val pagedList: LiveData<PagedList<T>>?,
    val networkState: LiveData<RepoState>? = null,
    val refreshState: LiveData<RepoState>? = null,
    val refresh: () -> Unit = {},
    val retry: () -> Unit = {},
    val dbState: LiveData<RepoState>? = null
)