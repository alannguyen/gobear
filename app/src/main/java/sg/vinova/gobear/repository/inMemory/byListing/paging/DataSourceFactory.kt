package sg.vinova.gobear.repository.inMemory.byListing.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource.LoadParams
import retrofit2.Call
import sg.vinova.gobear.response.ListObjectResponse
import sg.vinova.gobear.response.ObjectResponse
import java.util.concurrent.Executor

class DataSourceFactory<T, K>(
    private val originalCall: Call<ListObjectResponse<T>>,
    private val retryExecutor: Executor,
    private val retryCall: Call<ObjectResponse<K>>,
    private val loadAfterCall: (params: LoadParams<Int>) -> Call<ListObjectResponse<T>>,
    private val handleResponseFunc: ((params: List<T>?) -> List<Any>?)? = null
) : DataSource.Factory<Int, Any>() {
    val sourceLiveData = MutableLiveData<PageKeyedDataSource<T, K>>()
    override fun create(): DataSource<Int, Any> {
        val source = PageKeyedDataSource(
            originalCall,
            retryExecutor,
            retryCall,
            loadAfterCall,
            handleResponseFunc
        )
        sourceLiveData.postValue(source)
        return source
    }
}
