package sg.vinova.gobear.repository.inDb.byListing.paging

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.PagedList
import androidx.paging.PagingRequestHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sg.vinova.gobear.MainApplication
import sg.vinova.gobear.base.BaseDb
import sg.vinova.gobear.constant.*
import sg.vinova.gobear.exception.NetworkErrorException
import sg.vinova.gobear.repository.RepoState
import sg.vinova.gobear.response.ListObjectResponse
import sg.vinova.gobear.response.ObjectResponse
import sg.vinova.gobear.util.createStatusLiveData
import sg.vinova.gobear.vo.feature.auth.UserAuthentication
import java.util.concurrent.Executor

const val DEFAULT_PAGE = 1

class WebServiceByListingPagingCallback<T, K>(
    private val webserviceCall: (Pair<Int, Int>) -> Call<ListObjectResponse<T>>,
    private val handleResponseFunc: ((params: List<T>?) -> List<Any>?)? = null,
    private val handleFinalResponseFunc: (List<Any?>) -> Unit,
    private val refreshFunc: (((params: List<T>?) -> List<Any>?)?, Response<ListObjectResponse<T>>) -> Unit,
    private val networkIOExecutor: Executor,
    private var retryCall: Call<ObjectResponse<K>>? = null,
    private val db: BaseDb?
) : PagedList.BoundaryCallback<Any>() {

    val helper = PagingRequestHelper(networkIOExecutor)
    val networkState = helper.createStatusLiveData()
    val dbState = MutableLiveData<RepoState>()
    val refreshTrigger = MutableLiveData<Unit>()
    val refreshState = Transformations.switchMap(refreshTrigger) {
        refresh(handleResponseFunc)
    }!!
    private var curPage = DEFAULT_PAGE

    override fun onZeroItemsLoaded() {
        helper.runIfNotRunning(PagingRequestHelper.RequestType.INITIAL) {
            webserviceCall(Pair(curPage, PAGE_SIZE)).enqueue(
                createWebserviceCallback(
                    it,
                    PagingRequestHelper.RequestType.INITIAL
                )
            )
        }
    }

    override fun onItemAtEndLoaded(itemAtEnd: Any) {
        helper.runIfNotRunning(PagingRequestHelper.RequestType.AFTER) {
            webserviceCall(Pair(curPage, PAGE_SIZE)).clone().enqueue(
                createWebserviceCallback(
                    it, PagingRequestHelper.RequestType.AFTER
                )
            )
        }
    }

    private fun insertItemsIntoDb(
        response: List<Any?>?,
        callback: PagingRequestHelper.Request.Callback
    ) {
        networkIOExecutor.execute {
            val result = db.runCatching {
                dbState.postValue(RepoState.LOADING)
                handleFinalResponseFunc(response ?: return@execute)
            }
            result.onFailure {
                dbState.postValue(RepoState.error(it.message))
                callback.recordFailure(it)
            }
            result.onSuccess {
                dbState.postValue(RepoState.LOADED)
                callback.recordSuccess()
            }.getOrThrow()
        }
    }

    override fun onItemAtFrontLoaded(itemAtFront: Any) {}

    private fun createWebserviceCallback(
        it: PagingRequestHelper.Request.Callback,
        type: PagingRequestHelper.RequestType
    ): Callback<ListObjectResponse<T>> {
        return object : Callback<ListObjectResponse<T>> {
            override fun onFailure(
                call: Call<ListObjectResponse<T>>,
                t: Throwable
            ) {
                val errorMessage = NetworkErrorException().errors(t).message
                val errorList = errorMessage?.split(" - ")
                if (errorList?.get(1)?.toInt() == NO_INTERNET_CONNECTION || errorList?.get(1)?.toInt() == NO_SERVER_CONNECTION) {
                    val finalThrow = Throwable(errorMessage)
                    it.recordFailure(NetworkErrorException().errors(finalThrow))
                } else
                    retryCall?.enqueue(RetryCallback(it, type, t.message))
            }

            override fun onResponse(
                call: Call<ListObjectResponse<T>>,
                response: Response<ListObjectResponse<T>>
            ) {
                when {
                    (response.body()?.code == OAUTH_TOKEN_INVALID_OR_EXPIRED ||
                            response.body()?.code == ACCESS_TOKEN_NOT_PROVIDE ||
                            response.body()?.code == ACCESS_TOKEN_INVALID ||
                            response.body()?.code == REFRESH_TOKEN_INVALID) -> {
                        retryCall?.clone()?.enqueue(RetryCallback(it, type, response.body()?.message))
                    }
                    //Success
                    response.body()?.code == null && response.body()?.message == null -> {
                        networkIOExecutor.execute {
                            val finalData = if (handleResponseFunc != null) handleResponseFunc.invoke(
                                response.body()?.data ?: return@execute
                            ) else response.body()?.data
                            insertItemsIntoDb(finalData ?: return@execute, it)
                            retryCall?.cancel()
                            curPage += 1
                        }
                    }
                    else -> {
                        it.recordFailure(Throwable(response.body()?.message))
                    }
                }

            }
        }
    }

    @MainThread
    private fun refresh(handleResponseFunc: ((params: List<T>?) -> List<Any>?)?): LiveData<RepoState> {
        val networkState = MutableLiveData<RepoState>()
        networkState.value = RepoState.LOADING
        webserviceCall(Pair(DEFAULT_PAGE, PAGE_SIZE)).enqueue(
            object : Callback<ListObjectResponse<T>> {
                override fun onFailure(call: Call<ListObjectResponse<T>>, t: Throwable) {
                    // retrofit calls this on main thread so safe to call set value
                    networkState.value = RepoState.error(t.message)
                }

                override fun onResponse(
                    call: Call<ListObjectResponse<T>>,
                    response: Response<ListObjectResponse<T>>
                ) {
                    networkIOExecutor.execute {
                        refreshFunc.invoke(handleResponseFunc, response)
                        // since we are in bg thread now, post the result.
                        networkState.postValue(RepoState.LOADED)
                    }
                }
            }
        )
        return networkState
    }

    inner class RetryCallback<K>(
        private val it: PagingRequestHelper.Request.Callback,
        private val type: PagingRequestHelper.RequestType,
        private val errorMessage: String?
    ) : Callback<ObjectResponse<K>> {
        override fun onFailure(call: Call<ObjectResponse<K>>?, t: Throwable?) {
            it.recordFailure(NetworkErrorException().errors(t, errorMessage))
        }

        override fun onResponse(
            executedCall: Call<ObjectResponse<K>>?,
            response: Response<ObjectResponse<K>>?
        ) {
            if ((response?.isSuccessful == true) && response.body()?.isSuccess() == true) {
                if (response.body() is ObjectResponse<K>)
                    MainApplication.instance.saveCurrentUser(response.body()?.data as? UserAuthentication)
                helper.runIfNotRunning(type) {
                    webserviceCall(Pair(curPage, PAGE_SIZE)).clone().enqueue(
                        createWebserviceCallback(
                            it, type
                        )
                    )
                }
                retryCall = null
            } else {
                it.recordFailure(Throwable(errorMessage ?: SERVER_ERROR))
            }
        }
    }
}