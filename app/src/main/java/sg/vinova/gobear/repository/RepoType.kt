package sg.vinova.gobear.repository

interface RepoType {
    enum class Type {
        DB,
        IN_MEMORY_BY_OBJECT,
        IN_MEMORY_BY_LISTING_NON_PAGING,
        IN_MEMORY_BY_LISTING_PAGING
    }
}