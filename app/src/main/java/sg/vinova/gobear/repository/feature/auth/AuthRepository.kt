package sg.vinova.gobear.repository.feature.auth

class AuthRepository {
    companion object {
        fun usernameValidator(username: String?): Boolean {
            return username?.toLowerCase() == "gobear"
        }

        fun passwordValidator(password: String?): Boolean {
            return password == "GoBearDemo"
        }
    }
}