package sg.vinova.gobear.repository.inMemory.byListing.nonPaging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sg.vinova.gobear.MainApplication
import sg.vinova.gobear.constant.NO_INTERNET_CONNECTION
import sg.vinova.gobear.constant.NO_SERVER_CONNECTION
import sg.vinova.gobear.constant.UNKNOWN_ERROR
import sg.vinova.gobear.exception.NetworkErrorException
import sg.vinova.gobear.ext.toListingBundle
import sg.vinova.gobear.repository.ListingBundle
import sg.vinova.gobear.repository.RepoState
import sg.vinova.gobear.response.ListObjectResponse
import sg.vinova.gobear.response.ObjectResponse
import sg.vinova.gobear.vo.feature.auth.UserAuthentication
import sg.vinova.rssconverterfactory.RssFeed

class RetrofitByListingLiveData<T, K>(
    private val pageSize: Int = 0,
    private val call: Call<T>,
    private val request: MutableLiveData<ListingBundle<Any?>?>? = null,
    private var retry: Retry<ObjectResponse<K>>? = null,
    private val handleResponseFunc: ((params: T?) -> List<Any>?)? = null
) : MutableLiveData<ListObjectResponse<Any>>(), Callback<T> {

    val networkState = MutableLiveData<RepoState>()

    fun run() {
        request?.postValue(
            ListingBundle(
                pagedList = MutableLiveData<PagedList<Any?>>(),
                networkState = networkState,
                retry = { onActive() },
                refresh = { onActive() })
        )
        onActive()
    }

    override fun onActive() {
        //Default is main thread
        networkState.postValue(RepoState.LOADING)
        call.clone().enqueue(this)
    }

    override fun onFailure(executedCall: Call<T>?, t: Throwable?) {
        val error = NetworkErrorException().errors(t).message?.split(" - ")
        if (error?.get(1)?.toInt() == NO_INTERNET_CONNECTION || error?.get(1)?.toInt() == NO_SERVER_CONNECTION)
            networkState.postValue(RepoState.error(error[0], error[1].toInt()))
        else
            retry?.retryFunc?.clone()?.enqueue(RetryCallback(t?.message))
    }

    override fun onResponse(executedCall: Call<T>?, response: Response<T>?) {
        val responseData = if (response?.body() is RssFeed) response.body() as? RssFeed else return
        if (response.isSuccessful && (responseData?.data?.size != 0)) {
            networkState.postValue(RepoState.LOADED)
            val finalData = if (handleResponseFunc != null) handleResponseFunc.invoke(
                response.body() ?: return
            ) else responseData?.data

            request?.postValue(
                finalData?.toListingBundle(
                    pageSize = pageSize, networkState = networkState,
                    retryFunc = { onActive() },
                    refreshFunc = { onActive() })
            )

            retry?.retryFunc = null
        } else {
            if (responseData != null) {
                networkState.postValue(RepoState.LOADED)
            } else
                retry?.retryFunc?.clone()?.enqueue(RetryCallback(response.message(), response.code()))
        }
    }

    open class Retry<K>(var retryFunc: Call<K>? = null)

    inner class RetryCallback<K>(private val errorMessage: String?, private val errorCode: Int? = UNKNOWN_ERROR) :
        Callback<ObjectResponse<K>> {
        override fun onFailure(call: Call<ObjectResponse<K>>?, t: Throwable?) {
            networkState.postValue(RepoState.error(errorMessage ?: "unknown err", errorCode))
        }

        override fun onResponse(executedCall: Call<ObjectResponse<K>>?, response: Response<ObjectResponse<K>>?) {
            if ((response?.isSuccessful == true) && (response.body()?.isSuccess() == true)) {
                if (response.body() is ObjectResponse<K>)
                    MainApplication.instance.saveCurrentUser(response.body()?.data as? UserAuthentication)
                call.clone().enqueue(this@RetrofitByListingLiveData)
            } else {
                networkState.postValue(RepoState.error(errorMessage ?: "unknown err", errorCode))
            }
        }
    }
}