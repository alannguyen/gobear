package sg.vinova.gobear.repository

import sg.vinova.gobear.constant.UNKNOWN_ERROR

enum class Status {
    RUNNING,
    SUCCESS,
    FAILED
}

@Suppress("DataClassPrivateConstructor")
data class RepoState private constructor(
    val status: Status,
    val msg: String? = null,
    val code: Int? = 0
) {
    companion object {
        val LOADED = RepoState(Status.SUCCESS)
        val LOADING = RepoState(Status.RUNNING)
        fun error(msg: String?, code: Int? = UNKNOWN_ERROR) = RepoState(Status.FAILED, msg, code)
    }
}