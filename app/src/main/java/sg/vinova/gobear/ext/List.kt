package sg.vinova.gobear.ext

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import sg.vinova.gobear.constant.PAGE_SIZE
import sg.vinova.gobear.repository.ListingBundle
import sg.vinova.gobear.repository.RepoState
import sg.vinova.gobear.util.ListDataSourceFactory

fun Any.toListingBundle(
    pageSize: Int = PAGE_SIZE,
    networkState: LiveData<RepoState>? = null,
    dbState: LiveData<RepoState>? = null,
    retryFunc: () -> Unit = {},
    refreshFunc: () -> Unit = {}
): ListingBundle<Any?>? {
    if (this !is List<*>)
        return null
    val sourceFactory = ListDataSourceFactory(this)
    val pagedListConfig = PagedList.Config.Builder()
        .setEnablePlaceholders(false)
        .setPageSize(pageSize)
        .build()
    val pagedList = LivePagedListBuilder(sourceFactory, pagedListConfig)
        .build()

    return ListingBundle(
        pagedList = pagedList,
        networkState = networkState,
        dbState = dbState,
        retry = retryFunc,
        refresh = refreshFunc
    )
}