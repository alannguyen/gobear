package sg.vinova.gobear.ext

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.view.inputmethod.InputMethodManager
import pub.devrel.easypermissions.EasyPermissions
import sg.vinova.gobear.R
import sg.vinova.gobear.helper.SharedPreferencesHelper
import sg.vinova.gobear.vo.feature.auth.UserAuthentication
import java.io.IOException
import java.nio.charset.Charset

fun Context.saveUserPrefObj(user: UserAuthentication?) {
    val pref = getSharedPreferences("gobear", MODE_PRIVATE)
    SharedPreferencesHelper(pref).saveUser(user ?: return)
}

fun Context.getUserPrefObj(): UserAuthentication? {
    val pref = getSharedPreferences("gobear", MODE_PRIVATE)
    return SharedPreferencesHelper(pref).getUser()
}

fun Context.deleteUserPrefObj() {
    val pref = getSharedPreferences("GoBear", MODE_PRIVATE)
    pref.edit().clear().apply()
}

fun Context.hasReadStoragePermission(): Boolean {
    return EasyPermissions.hasPermissions(
        this,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )
}

fun Context.requestReadStoragePermission(requestCode: Int) {
    EasyPermissions.requestPermissions(
        this as? Activity
            ?: return, this.getString(R.string.read_storage_permission),
        requestCode, Manifest.permission.READ_EXTERNAL_STORAGE
    )
}

fun Context.hasCallPermission(): Boolean {
    return EasyPermissions.hasPermissions(this, Manifest.permission.CALL_PHONE)
}

fun Context.requestCallPermission(requestCode: Int) {
    return EasyPermissions.requestPermissions(
        this as? Activity
            ?: return, this.getString(R.string.call_permission),
        requestCode, Manifest.permission.CALL_PHONE
    )
}

fun Context.loadJsonFromAssest(fileName: String): String {
    val json: String?
    try {
        val inputStream = this.assets.open(fileName)
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        json = String(buffer, Charset.defaultCharset())
    } catch (ex: IOException) {
        ex.printStackTrace()
        return ""
    }

    return json
}

//Keyboard util
//fun Context.createFileFromJson(params: String, mJsonResponse: String) {
//    try {
//        FileWriter("/data/data/" + this.packageName + "/" + params).apply {
//            write(mJsonResponse)
//            flush()
//            close()
//        }
//    } catch (e: IOException) {
//        e.printStackTrace()
//    }
//}

fun Context.hideKeyboard(){
    val inputManager = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    // check if no view has focus:
    val v = (this as Activity).currentFocus ?: return

    inputManager.hideSoftInputFromWindow(v.windowToken, 0)
}