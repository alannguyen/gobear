package sg.vinova.gobear.ext

import android.util.Patterns

fun String.checkIsNotEmpty(): Boolean = this.trim().isNotEmpty()
fun String.checkEmail(): Boolean = this.trim().isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this.trim()).matches()
fun String.checkPassword(): Boolean = this.trim().length > 5