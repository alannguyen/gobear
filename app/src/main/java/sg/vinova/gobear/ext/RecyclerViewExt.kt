package sg.vinova.gobear.ext

import android.view.View
import sg.vinova.gobear.util.ItemClickSupport
import sg.vinova.gobear.util.OnItemClickListener
import sg.vinova.gobear.util.OnItemLongClickListener

fun androidx.recyclerview.widget.RecyclerView.setOnItemClickListener(listener: OnItemClickListener) {
    ItemClickSupport.addTo(this).setOnItemClickListener(listener)
}

fun androidx.recyclerview.widget.RecyclerView.setOnItemLongClickListener(listener: OnItemLongClickListener) {
    ItemClickSupport.addTo(this).setOnItemLongClickListener(listener)
}

fun androidx.recyclerview.widget.RecyclerView.setOnItemParentAndChildrenClickListener(
    listener: OnItemClickListener,
    messageQueue: ArrayList<(curItemView: View?) -> Unit>? = null
) {
    ItemClickSupport.addTo(this).setOnItemParentAndChildrenClickListener(listener, messageQueue)
}

fun androidx.recyclerview.widget.RecyclerView.setOnItemParentAndChildrenLongClickListener(
    listener: OnItemLongClickListener,
    messageQueue: ArrayList<(curItemView: View?) -> Unit>? = null
) {
    ItemClickSupport.addTo(this).setOnItemParentAndChildrenLongClickListener(listener, messageQueue)
}