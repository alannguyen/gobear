package sg.vinova.gobear.ext

import android.animation.ObjectAnimator
import android.animation.StateListAnimator
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.Toolbar
import androidx.core.view.children
import com.google.android.material.appbar.AppBarLayout
import sg.vinova.gobear.R

fun AppCompatActivity.setupToolbar(
    @LayoutRes toolbarLayoutId: Int?, @IdRes rootViewId: Int?, hasBack: Boolean = false,
    messageQueue: ArrayList<(activity: AppCompatActivity?, toolbar: Toolbar?) -> Unit>? = null,
    elevation: Float = 3.5F
) {
    var toolbarItem: View? = null
    if (toolbarLayoutId != null && rootViewId != null) {
        findViewById<ViewGroup>(rootViewId).apply {
            this.removeView(this.children.find { it.id == R.id.appBarLayout })
            toolbarItem = layoutInflater.inflate(toolbarLayoutId, this, false)
            this.addView(toolbarItem)
        }
    }
    val toolbar = toolbarItem?.findViewById<Toolbar>(R.id.toolbar)
    when (hasBack) {
        true -> toolbar?.findViewById<AppCompatImageButton>(R.id.ivBack)?.visibility = View.VISIBLE
        false -> toolbar?.findViewById<AppCompatImageButton>(R.id.ivBack)?.visibility = View.GONE
    }
    this.setSupportActionBar(toolbar)
    //end
    messageQueue?.forEach { it ->
        it.invoke(this, toolbar)
    }
    val layoutAppBar = toolbar?.parent as? AppBarLayout
    val stateListAnimator = StateListAnimator()
    stateListAnimator.addState(IntArray(0), ObjectAnimator.ofFloat(layoutAppBar, "elevation", elevation))
    layoutAppBar?.stateListAnimator = stateListAnimator
    toolbar?.requestLayout()
}

fun AppCompatActivity.removeToolbar(@IdRes rootViewId: Int?) {
    if (rootViewId != null) {
        findViewById<ViewGroup>(rootViewId)?.apply {
            removeView(children.find { it.id == R.id.appBarLayout })
            requestLayout()
        }
    }
}
