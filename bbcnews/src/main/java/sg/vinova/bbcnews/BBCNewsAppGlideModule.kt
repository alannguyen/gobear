package sg.vinova.bbcnews

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class BBCNewsAppGlideModule : AppGlideModule()