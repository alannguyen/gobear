package sg.vinova.bbcnews

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import sg.vinova.bbcnews.base.BaseActivity
import sg.vinova.bbcnews.databinding.ActivityMainBinding

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    override val tag = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    }

    override fun onSupportNavigateUp() = findNavController(R.id.frNavigator).navigateUp()
}