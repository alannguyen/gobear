package sg.vinova.bbcnews.helper

import android.app.Dialog
import android.view.View
import android.widget.TextView
import com.google.android.material.button.MaterialButton
import sg.vinova.bbcnews.R
import sg.vinova.bbcnews.base.BaseActivity
import sg.vinova.gobear.ext.addMessage
import sg.vinova.gobear.vo.PopUp

//error
fun BaseActivity.showErrorPopUp(message: String?) {
    //Init message queue
    val messageQueue = ArrayList<(view: View?, dialog: Dialog?) -> Unit>()
    messageQueue.addMessage(function = closePopUp(R.id.btnClose, this))
    messageQueue.addMessage(function = updateContent(message))
    //end

    val popup = PopUp(R.layout.base_dialog_error, messageQueue)
    this.showPopup(popup)
}

private fun closePopUp(viewId: Int, baseActivity: BaseActivity): (view: View?, dialog: Dialog?) -> Unit =
    { view, _ ->
        view?.findViewById<MaterialButton>(viewId)?.setOnClickListener {
            baseActivity.closePopup()
        }
    }

private fun updateContent(message: String?): (view: View?, dialog: Dialog?) -> Unit = { view, _ ->
    view?.findViewById<TextView>(R.id.tvDetailStatus)?.text = message
}