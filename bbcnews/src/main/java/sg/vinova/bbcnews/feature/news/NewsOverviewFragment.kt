package sg.vinova.bbcnews.feature.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import org.jetbrains.anko.doAsyncResult
import org.jetbrains.anko.uiThread
import org.jsoup.Jsoup
import sg.vinova.bbcnews.R
import sg.vinova.bbcnews.base.BaseActivity
import sg.vinova.bbcnews.base.BaseFragment
import sg.vinova.bbcnews.databinding.FragmentNewsOverviewBinding
import sg.vinova.gobear.builder.toolbarFunctionQueue
import sg.vinova.gobear.ext.setupToolbar
import sg.vinova.gobear.vo.feature.news.News

class NewsOverviewFragment : BaseFragment() {

    private lateinit var binding: FragmentNewsOverviewBinding
    override var toolbarViewParentId = R.id.clMainContainer
    private lateinit var article: News

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_overview, container, false)
        article = arguments?.getParcelable("article") ?: News()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolbar()

        //show loading
        (activity as? BaseActivity)?.showPopup(null)

        doAsyncResult {
            val doc = Jsoup.connect(article.item?.link).get()
                .select("div.story-body__inner")
            val contents = doc.select("p")
            uiThread {
                val contentNews = StringBuilder()
                contents.forEach { content ->
                    contentNews.append("${content.text()}\n\n")
                }
                binding.news = article.item
                binding.content = contentNews.toString()
                Glide.with(binding.root.context)
                    .load(article.item?.image)
                    .into(binding.ivOverview)

                //show loading
                (activity as? BaseActivity)?.closePopup()
            }
        }
    }

    private fun setUpToolbar() {
        (activity as? BaseActivity)?.setupToolbar(
            toolbarLayoutId = R.layout.toolbar_detail_news,
            rootViewId = toolbarViewParentId, hasBack = true,
            messageQueue = toolbarFunctionQueue {
                func { curActivity, toolbar ->
                    toolbar?.findViewById<AppCompatImageButton>(R.id.ivBack)?.setOnClickListener {
                        curActivity?.onBackPressed()
                    }
                    curActivity?.supportActionBar?.setDisplayShowTitleEnabled(false)
                }
            })
    }
}