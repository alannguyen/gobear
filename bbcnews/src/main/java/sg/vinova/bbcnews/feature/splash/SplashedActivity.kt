package sg.vinova.bbcnews.feature.splash

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivity
import sg.vinova.bbcnews.MainActivity
import sg.vinova.bbcnews.feature.auth.SignInActivity
import sg.vinova.bbcnews.feature.walkthrough.WalkThroughActivity
import sg.vinova.gobear.MainApplication

private const val ACTIVITY_AUTH = 0x000

class SplashedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        val userCredential = MainApplication.instance.currentUser
        when {
            userCredential?.isSkip == false || userCredential == null -> {
                startActivity<WalkThroughActivity>()
            }
            !isAuthenticated() -> {
                startActivityForResult(Intent(this, SignInActivity::class.java), ACTIVITY_AUTH)
            }
            else -> {
                startActivity(intentFor<MainActivity>())
            }
        }

        super.onCreate(savedInstanceState)
        finish()
    }

    private fun isAuthenticated(): Boolean {
        return (application as? MainApplication)?.currentUser?.token != null
    }

    private fun onAuthenticatedCallback(resultCode: Int) {
        when (resultCode) {
            Activity.RESULT_CANCELED -> finish()
            Activity.RESULT_OK -> recreate()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            ACTIVITY_AUTH -> onAuthenticatedCallback(resultCode)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}