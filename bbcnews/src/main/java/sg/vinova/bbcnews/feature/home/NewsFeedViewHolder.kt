package sg.vinova.bbcnews.feature.home

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import sg.vinova.bbcnews.databinding.ItemNewsBinding
import sg.vinova.gobear.vo.feature.news.News

class NewsFeedViewHolder(
    view: View,
    private val binding: ItemNewsBinding
) : RecyclerView.ViewHolder(view) {

    companion object {
        fun create(binding: ItemNewsBinding): NewsFeedViewHolder =
            NewsFeedViewHolder(binding.root, binding)
    }

    fun bind(item: News?) {
        binding.tvTitle.text = item?.item?.title
        binding.tvDescription.text = item?.item?.description
        binding.tvPublishDate.text = item?.item?.publishDate
        Glide.with(binding.root.context)
            .load(item?.item?.image)
            .into(binding.ivArticle)
    }
}