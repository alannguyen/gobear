package sg.vinova.bbcnews.feature.walkthrough

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.CheckBox
import androidx.databinding.DataBindingUtil
import androidx.leanback.app.OnboardingSupportFragment
import org.jetbrains.anko.startActivity
import sg.vinova.bbcnews.R
import sg.vinova.bbcnews.databinding.FragmentWalkthroughBinding
import sg.vinova.bbcnews.feature.auth.SignInActivity
import sg.vinova.gobear.MainApplication
import sg.vinova.gobear.ext.saveUserPrefObj
import sg.vinova.gobear.ext.visible
import sg.vinova.gobear.vo.feature.auth.UserAuthentication

private const val ANIMATION_DURATION: Long = 300

class WalkThroughFragment : OnboardingSupportFragment() {

    private lateinit var description: List<String>
    private lateinit var images: List<Int>
    private var contentAnimator: Animator? = null
    private lateinit var binding: FragmentWalkthroughBinding

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        description = resources.getStringArray(R.array.description).toList()
        images = listOf(R.drawable.intro1, R.drawable.intro2, R.drawable.intro3)
    }

    override fun getPageTitle(pageIndex: Int): CharSequence = description[pageIndex]

    override fun getPageDescription(pageIndex: Int): CharSequence = description[pageIndex]

    override fun onCreateForegroundView(inflater: LayoutInflater?, container: ViewGroup?): View? = null

    override fun onCreateBackgroundView(inflater: LayoutInflater?, container: ViewGroup?): View? =
        layoutInflater.inflate(R.layout.bg_intro, container, false)

    override fun getPageCount(): Int = description.size

    override fun onCreateContentView(inflater: LayoutInflater?, container: ViewGroup?): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_walkthrough, container, false)

        initView()

        binding.btnSkip.setOnClickListener {
            onFinishFragment()
        }

        binding.btnNext.setOnClickListener {
            if (currentPageIndex < 2)
                moveToNextPage()
            else
                onFinishFragment()
        }
        return binding.root
    }

    override fun onCreateAnimator(transit: Int, enter: Boolean, nextAnim: Int): Animator {
        val animators = ArrayList<Animator?>()
        contentAnimator = createFadeInAnimator(binding.ivHeader)
        animators.add(contentAnimator)
        val set = AnimatorSet()
        set.playTogether(animators)
        set.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                (binding.ivHeader.drawable as? AnimationDrawable)?.start()
            }
        })
        return set
    }

    override fun onPageChanged(newPage: Int, previousPage: Int) {
        if (contentAnimator != null)
            contentAnimator?.cancel()

        changeView(binding.ivHeader) {
            binding.ivHeader.setImageDrawable(resources.getDrawable(images[newPage], null))
        }

        changeView(binding.tvDescription) {
            binding.description = description[newPage]
        }

        changeDot(newPage, previousPage)
        visibleView(previousPage)
    }

    private fun initView() {
        binding.ivHeader.setImageDrawable(resources.getDrawable(images[0], null))
        binding.description = description[0]
        binding.position = 0
        binding.indicator.cbDot0.startAnimation(createFadeInScale())
    }

    private fun changeView(view: View?, callback: (() -> Unit)? = null) {
        (binding.ivHeader.drawable as? AnimationDrawable)?.stop()
        val fadeOut = createFadeOutAnimator(view)
        fadeOut.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                callback?.invoke()
            }
        })
        val fadeIn = createFadeInAnimator(view)
        fadeIn.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                (binding.ivHeader.drawable as? AnimationDrawable)?.start()
            }
        })
        val set = AnimatorSet()
        set.playSequentially(fadeOut, fadeIn)
        set.start()
        contentAnimator = set
    }

    private fun visibleView(previousPage: Int) {
        val view = view?.findViewById<View>(
            resources
                .getIdentifier("progress$previousPage", "id", requireContext().packageName)
        )
        view?.visible()
        view?.startAnimation(createProgressScale())
    }

    private fun changeDot(newPage: Int, previousPage: Int) {
        val curDot = view?.findViewById<CheckBox>(
            resources.getIdentifier(
                "cbDot$newPage",
                "id", requireContext().packageName
            )
        )

        val previousDot = view?.findViewById<CheckBox>(
            resources.getIdentifier(
                "cbDot$previousPage",
                "id", requireContext().packageName
            )
        )

        binding.position = newPage
        curDot?.startAnimation(createFadeInScale())
        previousDot?.startAnimation(createFadeOutScale())

    }

    private fun createFadeInAnimator(view: View?): Animator {
        return ObjectAnimator.ofFloat(view, View.ALPHA, 0.0f, 1.0f).setDuration(ANIMATION_DURATION)
    }

    private fun createFadeOutAnimator(view: View?): Animator {
        return ObjectAnimator.ofFloat(view, View.ALPHA, 0.5f, 0.0f).setDuration(ANIMATION_DURATION)
    }

    private fun createFadeInScale(): ScaleAnimation {
        val fadeIn = ScaleAnimation(
            1f, 1.5f, 1f, 1.5f,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
        )
        fadeIn.duration = 300
        fadeIn.fillAfter = true
        return fadeIn
    }

    private fun createFadeOutScale(): ScaleAnimation {
        val fadeOut = ScaleAnimation(
            1.5f, 1f, 1.5f, 1f,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
        )
        fadeOut.duration = 300
        fadeOut.fillAfter = true
        return fadeOut
    }

    private fun createProgressScale(): ScaleAnimation {
        val fadeIn = ScaleAnimation(0f, 1f, 1f, 1f)
        fadeIn.duration = 300
        fadeIn.fillAfter = true
        return fadeIn
    }

    override fun onFinishFragment() {
        val userCredential = MainApplication.instance.currentUser ?: UserAuthentication()
        userCredential.isSkip = true
        context?.saveUserPrefObj(userCredential)
        context?.startActivity<SignInActivity>()
        activity?.finish()
    }
}