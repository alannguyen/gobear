package sg.vinova.bbcnews.feature.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageButton
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import org.jetbrains.anko.startActivity
import sg.vinova.bbcnews.R
import sg.vinova.bbcnews.base.BaseActivity
import sg.vinova.bbcnews.base.BaseFragment
import sg.vinova.bbcnews.base.BasePagedListAdapter
import sg.vinova.bbcnews.databinding.FragmentHomeBinding
import sg.vinova.bbcnews.databinding.ItemNewsBinding
import sg.vinova.bbcnews.feature.auth.SignInActivity
import sg.vinova.gobear.Injector
import sg.vinova.gobear.MainApplication
import sg.vinova.gobear.base.BaseDiffUtilCallback
import sg.vinova.gobear.builder.toolbarFunctionQueue
import sg.vinova.gobear.ext.saveUserPrefObj
import sg.vinova.gobear.ext.setOnItemClickListener
import sg.vinova.gobear.ext.setupToolbar
import sg.vinova.gobear.repository.RepoType
import sg.vinova.gobear.viewmodel.news.NewsViewModel
import sg.vinova.gobear.vo.feature.news.News

class HomeFragment : BaseFragment() {

    private lateinit var binding: FragmentHomeBinding
    override var toolbarViewParentId = R.id.clMainContainer

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpToolbar()
        newsViewModel = getNewsViewModel()
        initAdapter()

        newsViewModel.getNews()
    }

    private fun setUpToolbar() {
        (activity as? BaseActivity)?.setupToolbar(
            toolbarLayoutId = R.layout.toolbar_home,
            rootViewId = toolbarViewParentId, hasBack = false,
            messageQueue = toolbarFunctionQueue {
                func { curActivity, toolbar ->
                    val btnLogout = toolbar?.findViewById<AppCompatImageButton>(R.id.btnLogout)
                    btnLogout?.setOnClickListener {
                        val userCredential = MainApplication.instance.currentUser
                        userCredential?.token = null
                        userCredential?.isSkip = true
                        curActivity?.saveUserPrefObj(userCredential)
                        curActivity?.startActivity<SignInActivity>()
                        curActivity?.finish()
                    }
                }
            })
    }

    //Test
    private lateinit var newsViewModel: NewsViewModel

    //init News adapter
    private fun onBindNewsViewHolderFunc(holder: RecyclerView.ViewHolder, item: Any?) =
        (holder as? NewsFeedViewHolder)?.bind(item as? News)

    private fun baseDiffUtilNewsCallback() = BaseDiffUtilCallback<Any>(
        areContentsTheSameFunc = { oldItem, newItem ->
            (oldItem as? News)?.newsId == (newItem as? News)?.newsId
        },
        getChangePayloadFunc = { oldItem, newItem ->
            if ((oldItem as? News)?.copy(
                    newsId = (newItem as? News)?.newsId ?: return@BaseDiffUtilCallback false
                ) == newItem
            ) {
                newItem
            } else {
                News()
            }
        }
    ) //end

    private fun initAdapter() {
        //init news feed adapter
        val newsFeedAdapter = BasePagedListAdapter(
            onBindViewHolderFunc = this::onBindNewsViewHolderFunc,
            baseDiffUtilCallback = this::baseDiffUtilNewsCallback,
            layoutId = R.layout.item_news,
            onCreateViewHolderFunc = { binding -> NewsFeedViewHolder.create(binding as ItemNewsBinding) }
        ) {
            newsViewModel.retry()
        }
        binding.rvNews.adapter = newsFeedAdapter
        //end

        bindEvent(newsFeedAdapter)
    }

    private fun bindEvent(newsAdapter: BasePagedListAdapter) {

        ////////////////////////Item////////////////////////
        newsViewModel.newsItemLiveData.observe(this, Observer { it ->
            it?.let {
                newsAdapter.submitList(it)
                binding.swNews.isRefreshing = false
            }
        })

        newsViewModel.networkState.observe(this, Observer {
            Log.d("alan", "Non-Paging networkState" + it?.toString())
            newsAdapter.setNetworkState(it)
        })

        newsViewModel.dbState.observe(this, Observer {
            Log.d("alan", "Non-Paging dbState" + it?.toString())
        })

        binding.rvNews.setOnItemClickListener { _, curItem, position, _ ->
            val bundle = Bundle()
            bundle.putParcelable("article", (newsAdapter.currentList?.get(position) as? News))
            curItem.findNavController().navigate(R.id.action_fragment_home_to_newsOverviewFragment, bundle)
        }

        binding.swNews.setOnRefreshListener {
            newsViewModel.getNews()
        }
        //end
    }

    //Get Singleton ViewModel
    private fun getNewsViewModel(): NewsViewModel {
        val repoType = RepoType.Type.IN_MEMORY_BY_LISTING_NON_PAGING
        val factory = Injector.provideNewsViewModelFactory(requireContext(), repoType)

        return ViewModelProviders.of(this, factory)
            .get(NewsViewModel::class.java)
    }
    //end
}