package sg.vinova.bbcnews.feature.walkthrough

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import sg.vinova.bbcnews.R

class WalkThroughActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_walkthrough)
    }
}