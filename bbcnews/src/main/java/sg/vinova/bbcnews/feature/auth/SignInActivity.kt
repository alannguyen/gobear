package sg.vinova.bbcnews.feature.auth

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import org.jetbrains.anko.intentFor
import sg.vinova.bbcnews.MainActivity
import sg.vinova.bbcnews.R
import sg.vinova.bbcnews.base.BaseActivity
import sg.vinova.bbcnews.databinding.ActivitySignInBinding
import sg.vinova.bbcnews.helper.showErrorPopUp
import sg.vinova.gobear.Injector
import sg.vinova.gobear.MainApplication
import sg.vinova.gobear.ext.checkPassword
import sg.vinova.gobear.ext.hideKeyboard
import sg.vinova.gobear.ext.saveUserPrefObj
import sg.vinova.gobear.param.feature.auth.SignInParam
import sg.vinova.gobear.viewmodel.auth.SignInViewModel
import sg.vinova.gobear.vo.feature.auth.UserAuthentication

class SignInActivity : BaseActivity() {
    override val tag = SignInActivity::class.java.simpleName

    private lateinit var binding: ActivitySignInBinding
    private lateinit var signInViewModel: SignInViewModel
    private var isRemember: Boolean = false

    override fun onStart() {
        super.onStart()
        if (MainApplication.instance.currentUser != null)
            MainApplication.instance.updateCurrentUser()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in)
        signInViewModel = getSignInViewModel()
        binding.viewModel = signInViewModel

        initView()
        bindEvent()

        super.onCreate(savedInstanceState)
    }

    private fun initView() {
        val userCredential = MainApplication.instance.currentUser
        signInViewModel.password.set((if (userCredential?.isRemember == true) userCredential.password else ""))
        signInViewModel.username.set((if (userCredential?.isRemember == true) userCredential.username else ""))
        binding.scRememberMe.isChecked = userCredential?.isRemember ?: false
    }

    private fun bindEvent() {
        binding.btnLogin.setOnClickListener {
            this.hideKeyboard()
            signIn()
        }

        //Remember me
        binding.scRememberMe.setOnCheckedChangeListener { _, isChecked ->
            isRemember = isChecked
        }

        //Login success
        signInViewModel.userLiveData.observe(this, Observer {
            val curUser = UserAuthentication(
                username = it.username,
                password = it.password,
                token = it.token,
                isRemember = it.isRemember,
                isSkip = true
            )
            saveUserPrefObj(curUser)
            startActivity(intentFor<MainActivity>())
            finish()
        })

        //Login failed
        signInViewModel.errorLiveData.observe(this, Observer {
            this.showErrorPopUp(it.msg.toString())
        })
    }

    private fun signIn() {
        if (checkValid()) {
            binding.root.requestFocus()
            signInViewModel.setLogin(
                signInParam = SignInParam(
                    binding.edtUsername.text?.toString() ?: "",
                    binding.edtPassword.text?.toString() ?: ""
                ),
                isRemember = binding.scRememberMe.isChecked
            )
        }
    }

    private fun checkValid() =
        when {
            !binding.edtPassword.text.toString().checkPassword() -> {
                binding.edtPassword.error = getString(R.string.invalid_password)
                binding.edtPassword.requestFocus()
                false
            }
            else -> true
        }

    private fun getSignInViewModel(): SignInViewModel {
        val factory = Injector.provideSignInViewModelFactory()
        return ViewModelProviders.of(this, factory)
            .get(SignInViewModel::class.java)
    }
}