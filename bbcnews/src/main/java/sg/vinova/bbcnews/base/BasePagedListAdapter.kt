package sg.vinova.bbcnews.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import sg.vinova.bbcnews.R
import sg.vinova.gobear.base.BaseDiffUtilCallback
import sg.vinova.gobear.repository.RepoState

class BasePagedListAdapter(
    private var onBindViewHolderFunc: (holder: RecyclerView.ViewHolder, item: Any?) -> Unit?,
    private var layoutId: Int,
    private var onCreateViewHolderFunc: (ViewDataBinding) -> RecyclerView.ViewHolder,
    baseDiffUtilCallback: () -> BaseDiffUtilCallback<Any>,
    private val retryCallback: () -> Unit
) : PagedListAdapter<Any, RecyclerView.ViewHolder>(itemCallback(baseDiffUtilCallback.invoke())) {

    private var networkState: RepoState? = null
    private lateinit var binding: ViewDataBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            R.layout.network_state_item -> BaseNetworkStateItemViewHolder.create(parent, retryCallback)

            layoutId -> {
                binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    layoutId, parent, false
                )
                onCreateViewHolderFunc.invoke(binding)
            }

            else -> throw IllegalArgumentException("unknown view type $viewType")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            layoutId -> onBindViewHolderFunc(holder, getItem(position))
            R.layout.network_state_item -> (holder as BaseNetworkStateItemViewHolder).bindTo(
                networkState
            )
        }
    }

    override fun getItemViewType(position: Int): Int =
        if (hasExtraRow() && position == itemCount - 1)
            R.layout.network_state_item
        else
            layoutId

    private fun hasExtraRow() = networkState != null && networkState != RepoState.LOADED

    override fun getItemCount(): Int = super.getItemCount() + if (hasExtraRow()) 1 else 0

    fun setNetworkState(newNetworkState: RepoState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }
}

private fun itemCallback(baseDiffUtilCallback: BaseDiffUtilCallback<Any>): DiffUtil.ItemCallback<Any> {
    return object : DiffUtil.ItemCallback<Any>() {

        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean =
            baseDiffUtilCallback.areItemsTheSameFunc?.invoke(oldItem, newItem) ?: (oldItem == newItem)

        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean =
            baseDiffUtilCallback.areContentsTheSameFunc(oldItem, newItem)

        override fun getChangePayload(oldItem: Any, newItem: Any): Any? =
            baseDiffUtilCallback.getChangePayloadFunc(oldItem, newItem)
    }
}