package sg.vinova.bbcnews.base

import androidx.appcompat.app.AppCompatActivity
import sg.vinova.gobear.util.PopupDialogFragment
import sg.vinova.gobear.vo.PopUp

abstract class BaseActivity : AppCompatActivity() {

    abstract val tag: String
    private var listOfPopupDialogFragment: ArrayList<PopupDialogFragment?>? = arrayListOf()

    fun showPopup(popup: PopUp?) {
        closePopup()
            val popupDialogFragment = PopupDialogFragment.newInstance(popup)
            popupDialogFragment.show(supportFragmentManager, PopupDialogFragment().tag)
            listOfPopupDialogFragment?.add(popupDialogFragment)
    }

    fun closePopup() {
        for (item in listOfPopupDialogFragment ?: arrayListOf()) {
            item?.dismissAllowingStateLoss()
        }
        listOfPopupDialogFragment?.clear()
    }
}