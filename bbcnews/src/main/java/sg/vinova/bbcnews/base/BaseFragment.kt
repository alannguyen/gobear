package sg.vinova.bbcnews.base

import androidx.fragment.app.Fragment
import sg.vinova.gobear.ext.removeToolbar

abstract class BaseFragment : Fragment() {
    open var toolbarViewParentId: Int = 0

    fun removeToolbar() {
        (activity as? BaseActivity)?.removeToolbar(if (toolbarViewParentId != 0) toolbarViewParentId else return)
    }
}