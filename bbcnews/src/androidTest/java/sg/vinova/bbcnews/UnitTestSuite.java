package sg.vinova.bbcnews;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import sg.vinova.bbcnews.auth.PasswordValidatorTest;
import sg.vinova.bbcnews.auth.SharedPreferencesHelperTest;
import sg.vinova.bbcnews.auth.UsernameValidatorTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({UsernameValidatorTest.class, PasswordValidatorTest.class, SharedPreferencesHelperTest.class})
public class UnitTestSuite {}
