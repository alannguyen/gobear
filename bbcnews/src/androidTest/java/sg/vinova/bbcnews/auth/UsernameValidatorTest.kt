package sg.vinova.bbcnews.auth

import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Test
import org.junit.runner.RunWith
import sg.vinova.gobear.repository.feature.auth.AuthRepository

@RunWith(AndroidJUnit4::class)
@SmallTest
class UsernameValidatorTest {

    @Test
    fun usernameValidator_Correct_ReturnsTrue() {
        assertThat(AuthRepository.usernameValidator("GoBear"), `is`(equalTo(true)))
    }

    @Test
    fun usernameValidator_CorrectAllLowerCase_ReturnsTrue() {
        assertThat(AuthRepository.usernameValidator("gobear"), `is`(equalTo(true)))
    }

    @Test
    fun usernameValidator_CorrectAllUpperCase_ReturnsTrue() {
        assertThat(AuthRepository.usernameValidator("GOBEAR"), `is`(equalTo(true)))
    }

    @Test
    fun usernameValidator_InvalidFormat_ReturnsFalse() {
        assertThat(AuthRepository.usernameValidator("gobear@gmail"), `is`(equalTo(false)))
    }

    @Test
    fun usernameValidator_InvalidFormat2_ReturnsFalse() {
        assertThat(AuthRepository.usernameValidator("gobear@gmail.com"), `is`(equalTo(false)))
    }

    @Test
    fun usernameValidator_EmptyString_ReturnsFalse() {
        assertThat(AuthRepository.usernameValidator(""), `is`(equalTo(false)))
    }

    @Test
    fun usernameValidator_NullEmail_ReturnsFalse() {
        assertThat(AuthRepository.usernameValidator(null), `is`(equalTo(false)))
    }

}
