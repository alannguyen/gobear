package sg.vinova.bbcnews.auth

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.gson.Gson
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import sg.vinova.gobear.helper.SharedPreferencesHelper
import sg.vinova.gobear.vo.Token
import sg.vinova.gobear.vo.feature.auth.UserAuthentication

@RunWith(AndroidJUnit4::class)
@SmallTest
class SharedPreferencesHelperTest {

    private val TEST_NAME = "gobear"
    private val TEST_PASSWORD = "GoBearDemo"
    private val TEST_REMMEMBER = false
    private val TEST_TOKEN = Token(TEST_NAME+TEST_PASSWORD)

    private lateinit var sharedPreferenceEntry: UserAuthentication
    private lateinit var mockSharedPreferencesHelper: SharedPreferencesHelper

    @Mock
    private lateinit var mockSharedPreferences: SharedPreferences
    @Mock
    private lateinit var mockEditor: SharedPreferences.Editor

    @Before
    fun initMocks() {
        // Create SharedPreferenceEntry to persist.
        sharedPreferenceEntry = UserAuthentication(username = TEST_NAME, password = TEST_PASSWORD, isRemember = TEST_REMMEMBER, token = TEST_TOKEN)

        // Create a mocked SharedPreferences.
        mockSharedPreferencesHelper = createMockSharedPreference()
    }

    @Test
    fun sharedPreferencesHelper_SaveAndReadGoBearUserInformation() {
        assertTrue(mockSharedPreferencesHelper.saveUser(sharedPreferenceEntry))

        val savedEntry = mockSharedPreferencesHelper.getUser()

        assertThat(sharedPreferenceEntry.username, `is`(equalTo(savedEntry?.username)))
        assertThat(sharedPreferenceEntry.password, `is`(equalTo(savedEntry?.password)))
        assertThat(sharedPreferenceEntry.isRemember, `is`(equalTo(savedEntry?.isRemember)))
        assertThat(sharedPreferenceEntry.token, `is`(equalTo(savedEntry?.token)))
    }

    private fun createMockSharedPreference(): SharedPreferencesHelper {
        mockSharedPreferences = ApplicationProvider.getApplicationContext<Context>().getSharedPreferences("gobear", MODE_PRIVATE)

        sharedPreferenceEntry = Gson().fromJson<UserAuthentication>(mockSharedPreferences.getString("user", null), UserAuthentication::class.java)

        mockEditor = mockSharedPreferences.edit()
        mockEditor.commit()

        return SharedPreferencesHelper(mockSharedPreferences)
    }
}
