package sg.vinova.bbcnews.auth

import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Test
import org.junit.runner.RunWith
import sg.vinova.gobear.repository.feature.auth.AuthRepository

@RunWith(AndroidJUnit4::class)
@SmallTest
class PasswordValidatorTest {

    @Test
    fun usernameValidator_Correct_ReturnsTrue() {
        assertThat(AuthRepository.passwordValidator("GoBearDemo"), `is`(equalTo(true)))
    }

    @Test
    fun usernameValidator_CorrectAllLowerCase_ReturnsFalse() {
        assertThat(AuthRepository.passwordValidator("gobeardemo"), `is`(equalTo(false)))
    }

    @Test
    fun usernameValidator_CorrectAllUpperCase_ReturnsFalse() {
        assertThat(AuthRepository.passwordValidator("GOBEARDEMO"), `is`(equalTo(false)))
    }

    @Test
    fun usernameValidator_EmptyString_ReturnsFalse() {
        assertThat(AuthRepository.passwordValidator(""), `is`(equalTo(false)))
    }

    @Test
    fun usernameValidator_NullEmail_ReturnsFalse() {
        assertThat(AuthRepository.passwordValidator(null), `is`(equalTo(false)))
    }

}
