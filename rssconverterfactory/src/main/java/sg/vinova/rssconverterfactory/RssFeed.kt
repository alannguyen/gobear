package sg.vinova.rssconverterfactory

data class RssFeed(var data: List<RssItem>? = null)